$(function(){        
    $("[data-toggle='tooltip']").tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 2000
    });
    $('#ASubsistenciaModal').on('show.bs.modal', function(e){
      console.log("el modal Agro Subsistencia esta mostrandose");
      $('#ASubsistenciaBTN').remove('btn-outline-success');
      $('#ASubsistenciaBTN').addClass('btn-primary');
      $('#ASubsistenciaBTN').prop('disable', true);
    });
    $('#ASubsistenciaModal').on('show.bs.modal', function(e){
      console.log('el modal Agro Subsistencia esta mostrandose');
    });
    $('#ASubsistenciaModal').on('hide.bs.modal', function(e){
      console.log('el modal Agro Subsistencia esta oculto');
    });
    $('#ASubsistenciaModal').on('hidden.bs.modal', function(e){
      console.log('el modal Agro Subsistencia esta oculto');
    });
  });